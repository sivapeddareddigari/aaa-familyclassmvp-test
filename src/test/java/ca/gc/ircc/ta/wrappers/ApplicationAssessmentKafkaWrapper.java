package ca.gc.ircc.ta.wrappers;

import ca.gc.ircc.ta.buildingblocks.ReadTestdata;
import ca.gc.ircc.ta.buildingblocks.RemoteConnectUtils;
import ca.gc.ircc.ta.buildingblocks.RestAssuredUtil;
import io.restassured.response.Response;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;


public class ApplicationAssessmentKafkaWrapper {
    long Sdate;
    long FDate;
    final static Logger perf = Logger.getLogger("perf");
    final static Logger file = Logger.getLogger("file");
    boolean scenarioStatus = true;
    public static final String pattern = "dd/MM/yyyy HH:mm:ss SS";
    public static final SimpleDateFormat format = new SimpleDateFormat(pattern);
    final static Logger logger = Logger.getLogger(apitestingwrapper.class);
    RemoteConnectUtils fcutils = new RemoteConnectUtils();
    ReadTestdata rt = new ReadTestdata();
    RestAssuredUtil rut = new RestAssuredUtil();
    Response res;
    String ScenarioDescription;
    String DataFileLocalFolder;
    String WaitNumber;
    String ScenarioType;
    String InputTopicName;
    String OutputTopicName;
    String InputTopicAvroSchemaDefFile;
    String OutputTopicAvroSchemaDefFile;
    String InputTopicPayloadFile;
    String OutputTopicPayloadFile;
    String DBConnectionString;
    String DBCollectionName;




    @Parameters({"ScenarioName", "TestDataFileName"})

    @BeforeClass
    public void BootStrap(String ScenarioName, String TestDataFileName) {
        try {
            BasicConfigurator.configure();
            HashMap<String, String> ScenarioData = new HashMap<String, String>();
            String log4jConfPath = "resources/log4j.properties";
            PropertyConfigurator.configure(log4jConfPath);

            Sdate = System.currentTimeMillis();

            if (TestDataFileName.contains(".xlsx")) {
                XSSFWorkbook workbook;
                FileInputStream finput = null;
                XSSFSheet sheet;
                File src = new File(TestDataFileName);

                try {
                    finput = new FileInputStream(src);

                    System.out.println(".....finput initialized");

                    workbook = new XSSFWorkbook(finput);
                    System.out.println(".....Workbook initialized");
                    sheet = workbook.getSheet("Initial");
                    System.out.println(".....initial Sheet initialized");

                    ScenarioData = rt.getScenarioData(workbook, sheet, ScenarioName);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (TestDataFileName.contains(".json") || TestDataFileName.contains(".JSON") ) {
                try {
                    ScenarioData = rt.readInputFromJSON("Queue", TestDataFileName, ScenarioName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            DataFileLocalFolder= ScenarioData.get("DataFileLocalFolder");
            WaitNumber = ScenarioData.get("WaitNumber");
            ScenarioType= ScenarioData.get("ScenarioType");
            InputTopicName= ScenarioData.get("InputTopicName");
            OutputTopicName = ScenarioData.get("OutputTopicName");
            InputTopicAvroSchemaDefFile= ScenarioData.get("InputTopicAvroSchemaDefFile");
            OutputTopicAvroSchemaDefFile = ScenarioData.get("OutputTopicAvroSchemaDefFile");
            InputTopicPayloadFile = ScenarioData.get("InputTopicPayloadFile");
            OutputTopicPayloadFile = ScenarioData.get("OutputTopicPayloadFile");
            DBConnectionString = ScenarioData.get("DBConnectionString");
            DBCollectionName = ScenarioData.get("DBCollectionName");

            ScenarioDescription = ScenarioData.get("ScenarioDescription");
            logger.info("Scenarios which is Running ::" + ScenarioDescription);

            //Place the message on to Topic


        } catch (Exception e) {

        }

    }

    @AfterClass
    public void Afterclassfunctionhere() {
        if (scenarioStatus) {

            FDate = System.currentTimeMillis();
            System.err.println("total time taken ~:" + (FDate - Sdate));
            perf.info(
                    ScenarioDescription + " | " + this.getClass().getName() + " | " + (FDate - Sdate));
        }

    }

    @Test(priority = 1, enabled = true)
    public void CheckEventinTopic() {
        //logger.info("Validating the "+ OutputContentType + " with expected structure...");
    }

    @Test(priority = 2, enabled = true)
    public void CheckEventConsumption() {

    }

    @Test(priority = 3, enabled = true)
    public void CheckDocumentInCollection() {

    }


}
