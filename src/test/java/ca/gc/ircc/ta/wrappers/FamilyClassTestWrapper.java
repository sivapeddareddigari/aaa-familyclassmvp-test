package ca.gc.ircc.ta.wrappers;

import ca.gc.ircc.ta.buildingblocks.MongodbUtils;
import ca.gc.ircc.ta.buildingblocks.ReadTestdata;
import ca.gc.ircc.ta.buildingblocks.RemoteConnectUtils;
import ca.gc.ircc.ta.buildingblocks.RestAssuredUtil;
import com.jcraft.jsch.ChannelSftp;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCursor;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import javax.jms.Destination;
import javax.jms.JMSContext;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import static org.testng.Assert.assertEquals;

public class FamilyClassTestWrapper {
    public static final String pattern = "dd/MM/yyyy HH:mm:ss SS";
    public static final SimpleDateFormat format = new SimpleDateFormat(pattern);
    final static Logger logger = Logger.getLogger(FamilyClassTestWrapper.class);
    public FTPClient fc;
    public String hostname;
    public int port;
    public String username;
    public String password;
    public ChannelSftp sfCnl;
    public JMSContext queueConnectionContext;
    public Destination dataSendToQueue;
    RemoteConnectUtils fcutils = new RemoteConnectUtils();
    ReadTestdata rt = new ReadTestdata();
    RestAssuredUtil rut = new RestAssuredUtil();
    MongodbUtils mt = new MongodbUtils();
    Response res;
    String BaseURI;
    String BasePath;
    String RequestType;
    String ContentType;
    String OutputContentType;
    String InputJSONFilePath;
    String MongoDBparams;
    String[] DBParams;
    String MongoDBQueryParams;
    String[] QueryParams;
    String MongoDBReturnParams;
    String[] ReturnParams;
    String ScenarioType;
    String ValidateResponseString;


    String ScenarioDescription;
    String EndpointName;
    String ResponseJSONSchemaFilePath;
    FileInputStream fis;
    HashMap<String, String> QueryData = new HashMap<>();


    long Sdate;
    long FDate;
    final static Logger perf = Logger.getLogger("perf");
    final static Logger file = Logger.getLogger("file");
    boolean scenarioStatus = true;

    @Parameters({"ScenarioName", "TestDataFileName"})

    @BeforeClass
    public void BootStrap(String ScenarioName, String TestDataFileName) {

        try {

            BasicConfigurator.configure();
            HashMap<String, String> ScenarioData = new HashMap<>();



            String log4jConfPath = "resources/log4j.properties";
            PropertyConfigurator.configure(log4jConfPath);

            Sdate = System.currentTimeMillis();

            if (TestDataFileName.contains(".xlsx")) {
                XSSFWorkbook workbook;
                FileInputStream finput;
                XSSFSheet sheet;
                File src = new File(TestDataFileName);

                try {
                    finput = new FileInputStream(src);

                    logger.info(".....Fileinput - initialized");

                    workbook = new XSSFWorkbook(finput);
                    logger.info(".....Workbook - initialized");
                    sheet = workbook.getSheet("Initial");
                    logger.info(".....Scenario data Sheet - initialized");

                    ScenarioData = rt.getScenarioData(workbook, sheet, ScenarioName);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (TestDataFileName.contains(".JSON")) {
                try {
                    ScenarioData = rt.readInputFromJSON("Queue", TestDataFileName, ScenarioName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            BaseURI = ScenarioData.get("BaseURI");
            BasePath = ScenarioData.get("BasePath");
            RequestType = ScenarioData.get("RequestType");
            ContentType = ScenarioData.get("ContentType");
            OutputContentType = ScenarioData.get("OutputContentType");
            InputJSONFilePath = ScenarioData.get("InputJSONFilePath");

            ScenarioDescription = ScenarioData.get("ScenarioDescription");
            EndpointName = ScenarioData.get("EndpointName");
            MongoDBparams = ScenarioData.get("MongoDBparams");
            DBParams = rt.getparams(MongoDBparams);
            MongoDBQueryParams = ScenarioData.get("MongoDBQueryParams");
            QueryParams = rt.getparams(MongoDBQueryParams);
            MongoDBReturnParams = ScenarioData.get("MongoDBReturnParams");
            ReturnParams = rt.getparams(MongoDBReturnParams);
            ScenarioType = ScenarioData.get("ScenarioType");
            ValidateResponseString= ScenarioData.get("ValidateResponseString");
            logger.info("Scenario which is Running ::" + ScenarioDescription);
            logger.info("Endpoint ::" + EndpointName);
            fis = new FileInputStream(InputJSONFilePath);


            logger.info("Requesting URI: " + BaseURI + BasePath + " using: " + RequestType + "  method...");
            res = rut.getResponse(BaseURI, BasePath, RequestType, ContentType, fis);
            logger.info("...Responded");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public void Afterclassfunctionhere() {
        if (scenarioStatus) {

            FDate = System.currentTimeMillis();
            logger.info("Total time taken ~:" + (FDate - Sdate));
            perf.info(
                    EndpointName + " | " + ScenarioDescription + " | " + this.getClass().getName() + " | " + (FDate - Sdate));
        }

    }

    @Test(priority = 1, enabled = true)
    public void CheckResponseCode() {
        logger.info("Checking the response code...");
        try {

            logger.info("...response code is: " + res.getStatusCode());
            assertEquals(res.getStatusCode(), 202);


        } catch (Exception e) {
            logger.info("ERR...response code is: " + res.getStatusCode());
            Assert.fail();

        }
    }

    @Test(priority = 2, enabled = true)
    public void CheckResponseContentType() {
        logger.info("Validating the response ContentType...");

        try {

            String contentType = res.header("content-Type");
            String KafkaKey =  res.header("kafka.key");
            assertEquals(true, contentType.equalsIgnoreCase("application/json"));
            logger.info("...validated");
            logger.info("Content-Type value: " + contentType);
            logger.info("Kafka.key value: " + KafkaKey);


        } catch (Exception ex) {
            logger.info("ERR...validation failed");

            Assert.fail();

        }

    }

    @Test(priority = 3, enabled = true)
    public void CheckResponseContent() {
        logger.info("Validating the Response content...");

        try {


            ResponseBody body = res.getBody();
            System.out.println(body.asString());
            System.out.println(ValidateResponseString);
            //assertEquals(true, body.asString().contains(ValidateResponseString));
            assertEquals(body.asString(), ValidateResponseString);

            logger.info("...validated");
            logger.info(body.asString());


        } catch (Exception ex) {
            logger.info("ERR...validation failed");
            Assert.fail();

        }

    }



    @Test(priority = 4, enabled = true)
    public void validateAtMongoDB() {
        String CorrelationID;
        BasicDBObject bo = new BasicDBObject();
        String DocumentConfirmation;


        logger.info("Validating document in MongoDb Collection ...");

        try {

            MongoClient client = mt.mongoClient(DBParams[0]);
            MongoCursor<String> dbsCursor = client.listDatabaseNames().iterator();

            //get querymap

            QueryData=rt.getQueryMap(QueryParams,InputJSONFilePath);
            //get formattedQuery
            bo=mt.getFormattedQuery("and",QueryData);

            //getDocumentFieldfromcollection
            logger.info("fetching "+ ReturnParams[0] +" from MongoDb: " + DBParams[1] +" Collection: "+ DBParams[2] +"...");
            DocumentConfirmation=mt.getDocumentFieldFromCollection(client,DBParams[1],DBParams[2],bo,ReturnParams[0]);

            if(DocumentConfirmation.length() >0){
                logger.info("....Document found for "+ ReturnParams[0]+": " + DocumentConfirmation );
                Assert.assertTrue(true);

            }else{
                logger.info("....Document does not exist for "+ ReturnParams[0] );
                Assert.fail();
            }


        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Exception...validation failed");
            Assert.fail();

        }

    }

}


