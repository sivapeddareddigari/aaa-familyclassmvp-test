package ca.gc.ircc.ta.buildingblocks;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MongodbUtils {
    public MongoClient mongoClient(String ConnectionURI) {

        return new MongoClient(new MongoClientURI(ConnectionURI));
    }

    public String getDocumentFieldFromCollection(MongoClient mc, String db, String col, BasicDBObject formattedQuery, String ReturnField) {
        String ReturnedValues = "";

        MongoCursor<String> dbsCursor = mc.listDatabaseNames().iterator();

        MongoDatabase database = mc.getDatabase(db);
        MongoCollection<Document> collection = database.getCollection(col);

       // FindIterable<document> cursor = restaurants.find().sort(new BasicDBObject("stars",-1)).limit(5);
       // MongoCursor<document> iterator = cursor.iterator();
        //Document document = collection.find().first();

        FindIterable<Document> docs = collection.find(formattedQuery);

        System.out.println(collection.countDocuments());



        if (docs == null) {
            return ReturnedValues;
        }

        for (Document doc : docs) {


            if (ReturnedValues == "") {
                ReturnedValues = ReturnedValues + doc.get(ReturnField);
            } else {
                ReturnedValues = ReturnedValues + "," + doc.get(ReturnField);
            }

        }
        return ReturnedValues;

    }

    public BasicDBObject getFormattedQuery(String andOr, HashMap<String, String> queryMap) {
        String andorStr = "$or";
        BasicDBObject andorQuery = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        //loopthough the Querymap and build the query string
        if (queryMap.size() > 1) {
            queryMap.forEach((key, value) -> obj.add(new BasicDBObject(key, value)));

            if (andOr.equalsIgnoreCase("and")) {
                andorStr = "$and";
            }

            andorQuery.put(andorStr, obj);
        } else {

            andorQuery.put(queryMap.keySet().toArray()[0].toString(), queryMap.get(queryMap.keySet().toArray()[0]));
        }
        return andorQuery;

    }

}
