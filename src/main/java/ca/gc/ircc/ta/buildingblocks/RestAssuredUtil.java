package ca.gc.ircc.ta.buildingblocks;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.InputStream;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import io.restassured.response.Response;

public class RestAssuredUtil {

	public Response getResponse(String BaseURI, String BasePath, String RequestType, String ContentType,
			FileInputStream fis) {
		Response res;
		res = given().baseUri(BaseURI).header("Content-type", ContentType).and().body(fis).when().post(BasePath).then()
				.extract().response();

		return res;

	}

	public boolean validateJSONStrcture(InputStream mySchema, String resBody) {

		try {

			JSONObject rawSchema = new JSONObject(new JSONTokener(mySchema));
			Schema schema = SchemaLoader.load(rawSchema);
			schema.validate(new JSONObject(resBody));
			return true;
		} catch (Exception e) {
			return false;

		}

	}
}
