package ca.gc.ircc.ta.buildingblocks;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.ResultSetMetaData;
import java.util.*;

/*import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;*/

public class ReadTestdata {
    final static Logger logger = Logger.getLogger(ReadTestdata.class);
    ArrayList<String> mystr = new ArrayList<>();
    ResultSetMetaData metaData1;
    /*
     * getValue(..) function is used to read the value of a screen element name from
     * the test data file This is an overloaded function with variable number of
     * parameters
     */

    public String getValue(XSSFWorkbook workbook, XSSFSheet sheet, String elementName, String Scenario) {
        String myStr = "";
        Sheet DataSheet = sheet;

        FormulaEvaluator formulaEval = workbook.getCreationHelper().createFormulaEvaluator();

        findLoop:
        for (int i = 1; i <= DataSheet.getLastRowNum(); i++) {

            if (DataSheet.getRow(i) != null) {
                // System.out.println("Element Name in GetValue method:"
                // +DataSheet.getRow(i).getCell(0).getStringCellValue().trim().equalsIgnoreCase(elementName));

                if (DataSheet.getRow(i).getCell(0).getStringCellValue().trim().equalsIgnoreCase(elementName)) {

                    myStr = formulaEval.evaluate(DataSheet.getRow(i).getCell(getColPos(DataSheet, Scenario)))
                            .formatAsString();

                    myStr = myStr.replaceAll("^\"|\"$", "");
                    break findLoop;
                }
            }
        }

        return myStr;

    }

    // This function returns the data elements of a scenario in to a hashmap and
    // avoids roundtrips to excell sheet

    public HashMap<String, String> getScenarioData(XSSFWorkbook workbook, XSSFSheet sheet, String Scenario)
            throws IOException {
        HashMap<String, String> ScenarioData = new HashMap<String, String>();
        int ColNum = 0;
        Iterator<Cell> cellIterator = sheet.getRow(0).cellIterator();
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();

            String text = cell.getStringCellValue();
            if (Scenario.equals(text)) {
                ColNum = cell.getColumnIndex();
                break;
            }

        }

        for (int i = 1; i < sheet.getLastRowNum() + 1; i++) {

            Cell c = sheet.getRow(i).getCell(0);
            Cell c1 = sheet.getRow(i).getCell(ColNum);

            ScenarioData.put(c.toString(), c1.toString());

        }

        return ScenarioData;
    }

    public HashMap<String, String> getScenarioData(String type, String TDFilename, String ScenarioName)
            throws IOException, ParseException {
        HashMap<String, String> ScenarioData = new HashMap<String, String>();
        JSONParser parser = new JSONParser();
        String RootFolder;
        try {
            RootFolder = TDFilename.substring(0, TDFilename.indexOf("/"));
            TDFilename = TDFilename.substring(TDFilename.indexOf("/"));

            Object obj = parser.parse(new FileReader(RootFolder + "\\" + TDFilename));
            JSONObject rootObject = (JSONObject) obj;
            JSONObject Scenarios = (JSONObject) rootObject.get("scenarios");

            JsonObject scenario1 = ((JsonObject) Scenarios).getJsonObject(ScenarioName);

            for (Object key : scenario1.keySet()) {
                // based on you key types
                String keyStr = (String) key;
                Object keyvalue = scenario1.get(keyStr);

                // Print key and value
                System.out.println("key: " + keyStr + " value: " + keyvalue);

            }

        } catch (IOException | ParseException ex) {

            ex.printStackTrace();
        }
        return ScenarioData;

    }

    /*
     * This is an internal function that returns the column position of an element
     */
    private int getColPos(Sheet sh, String Scenario) {
        int ColPos = 0;
        int noOfColumns = sh.getRow(0).getPhysicalNumberOfCells();
        for (int i = 0; i <= noOfColumns; ++i) {
            if (sh.getRow(0).getCell(i).getStringCellValue().trim().equalsIgnoreCase(Scenario)) {
                ColPos = i;
                break;

            }
        }

        return ColPos;
    }

    /*
     * This is an internal function that returns the Row position of an element
     */
    private int getRowPos(Sheet sh, String ElementName) {
        int RowPos = 0;
        int noOfRows = sh.getLastRowNum();
        for (int i = 1; i <= noOfRows; ++i) {
            if (sh.getRow(i).getCell(0).getStringCellValue().trim().equalsIgnoreCase(ElementName)) {
                RowPos = i;
                break;

            }
        }

        return RowPos;
    }

    /*
     * getBanAuthTableRows(..) function returns the number of stores in a table on
     * the screen. It assumes that all those stores in the screen are in sync with
     * the rows in test data file
     */
    public int getBanAuthTableRows(String areaName, XSSFSheet sheet) {

        int numRows = 0;
        Sheet DataSheet = sheet;

        for (int i = 1; i <= DataSheet.getLastRowNum(); i++) {

            if (DataSheet.getRow(i) != null) {

                if (DataSheet.getRow(i).getCell(0).getStringCellValue().trim().contains(areaName)) {
                    numRows = numRows + 1;

                }
            }
        }

        return numRows;

    }

    /*
     * setCellValue(..) method updates the target file with element value for a
     * given element at col/row position determined by elementName / Scenario)
     */
    public String setCellValue(String fileName, String sheetName, String elementName, String elementValue,
                               String Scenario) {

        try {

            File src = new File(fileName);
            FileInputStream finput = new FileInputStream(src);
            XSSFWorkbook workbook = new XSSFWorkbook(finput);
            XSSFSheet sheet = workbook.getSheet(sheetName);

            Cell cell1 = null;
            cell1 = sheet.getRow(getRowPos(sheet, elementName)).getCell(getColPos(sheet, Scenario));
            cell1 = sheet.getRow(getRowPos(sheet, elementName)).createCell(getColPos(sheet, Scenario));

            cell1.setCellValue(elementValue);

            finput.close();
            FileOutputStream foutput = new FileOutputStream(src);
            workbook.write(foutput);
            workbook.close();
            foutput.close();

        } catch (Exception e) {

            e.printStackTrace();
        }
        System.out.println("Updated the spreadsheet");

        return elementValue;
    }

    /*
     * getCellValue(..) method retrieves a cell value(element value) for a given
     * element at col/row position determined by elementName / Scenario)
     */
    public String getCellVal(String fileName, String sheetName, String elementName, String Scenario) {
        String celval = null;
        try {
            File src = new File(fileName);
            FileInputStream finput = new FileInputStream(src);
            XSSFWorkbook workbook = new XSSFWorkbook(finput);
            XSSFSheet sheet = workbook.getSheet(sheetName);

            Cell cell1 = null;
            cell1 = sheet.getRow(getRowPos(sheet, elementName)).getCell(getColPos(sheet, Scenario));
            celval = cell1.getStringCellValue();
            finput.close();
            workbook.close();
            // workbook.close();
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return celval;

    }

    public Map<String, String> getDSApps(XSSFWorkbook workbook, FileInputStream finput, String elementName,
                                         XSSFSheet sheet, String Scenario) {
        Map<String, String> hmap = new HashMap<String, String>();
        String myStr = "";
        Sheet DataSheet = sheet;

        FormulaEvaluator formulaEval = workbook.getCreationHelper().createFormulaEvaluator();

        for (int i = 1; i <= DataSheet.getLastRowNum(); i++) {

            if (DataSheet.getRow(i) != null) {
                // System.out.println("Element Name in GetValue method:"
                // +DataSheet.getRow(i).getCell(0).getStringCellValue().trim().equalsIgnoreCase(elementName));

                if (DataSheet.getRow(i).getCell(0).getStringCellValue().trim().contains(elementName)) {

                    myStr = formulaEval.evaluate(DataSheet.getRow(i).getCell(getColPos(DataSheet, Scenario)))
                            .formatAsString();

                    myStr = myStr.replaceAll("^\"|\"$", "");
                    if (!myStr.equals("NONE")) {
                        hmap.put(sheet.getRow(i).getCell(0).getStringCellValue().trim(), myStr);

                    }

                }
            }
        }

        return hmap;

    }

    //this function splits the "|" delimited hostString
    public String[] getHosts(String hostString) {
        String[] hosts = null;
        hosts = hostString.split("\\|");

        return hosts;

    }

    public String[] getparams(String hostString) {
        String[] params = null;
        params = hostString.split("\\~");

        return params;
    }

    // this function splits the "," delimited host in to Host, port, username,
    // password
    public String[] getHostDetails(String host) {
        String[] hostDetail = null;
        hostDetail = host.split("-|\\,");

        return hostDetail;

    }

    // this function splits the "&" delimited host in to list of files
    public String[] getfiles(String filelist) {
        String[] files = null;
        files = filelist.split("-|\\&");

        return files;

    }

    public String[] SplitString(String filelist) {
        String[] files = null;
        files = filelist.split("\\|");

        return files;

    }


    // this function will show error information to the developer
    public void formatErrorInfo(String scenarioData, Exception e) {
        HashMap<String, String> exceptionData = new HashMap<String, String>();
        exceptionData.put(scenarioData, e.getMessage());
        logger.info(exceptionData);
    }

    public HashMap<String, String> getDataFilesMD5Hash(String FolderPath, RemoteConnectUtils fcutils)
            throws IOException {
        HashMap<String, String> MD5HashMap = new HashMap<String, String>();
        File folder = new File(FolderPath);
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {

            if (file.isFile()) {

                // File DataFile = new File(FolderPath +"/"+ file);
                MD5HashMap.put(file.getName(), fcutils.getFileMD5Digest(file));

            }
        }

        return MD5HashMap;
    }

    public HashMap<String, String> getDataFilesMD5Hash(String FolderPath, RemoteConnectUtils fcutils, String[] filelist)
            throws IOException {
        HashMap<String, String> MD5HashMap = new HashMap<String, String>();


        // File folder = new File(FolderPath);
        for (int i = 0; i < filelist.length; i++) {
            File file = new File(FolderPath + filelist[i]);
            MD5HashMap.put(file.getName(), fcutils.getFileMD5Digest(file));

        }

        return MD5HashMap;
    }


    public HashMap<String, String> getQueryMap(String[] qp, String fname) {
        HashMap<String, String> QueryData = new HashMap<String, String>();
        JSONParser parser = new JSONParser();
        InputStream is;
        File jsonInputFile;

        try {
            if (System.getProperty("os.name").contains("Win")) {
                jsonInputFile = new File(fname.replace('/', '\\'));
            } else {
                jsonInputFile = new File(fname);
            }
            is = new FileInputStream(jsonInputFile);
            // Create JsonReader from Json.
            JsonReader reader = Json.createReader(is);
            // Get the JsonObject structure from JsonReader.
            JsonObject jo = reader.readObject();
            reader.close();
            // read string data

            for (String aqp : qp) {
                //find matching json value and add to hashmap
                if (getValuefromJSON(aqp, jo).length() > 0) {
                    QueryData.put(aqp, getValuefromJSON(aqp, jo));
                }
            }


        } catch (Exception e) {
            e.printStackTrace();

        }

        return QueryData;
    }

    public String getValuefromJSON(String k, JsonObject jo) {
        String valueStr = "";
        for (Iterator iterator = jo.keySet().iterator(); iterator.hasNext(); ) {
            String key = (String) iterator.next();
            if (k.equals(key)) {

                valueStr = jo.get(key).toString();
            }
        }
        return valueStr;
    }

    public HashMap<String, String> readInputFromJSON(String type, String TDFilename, String ScenarioName) {

        HashMap<String, String> scenariodataMap = new HashMap<String, String>();
        String RootFolder;

        RootFolder = TDFilename.substring(0, TDFilename.indexOf("/"));
        TDFilename = TDFilename.substring(TDFilename.indexOf("/"));

        File jsonInputFile = new File(RootFolder + "\\" + TDFilename);
        InputStream is;

        try {
            is = new FileInputStream(jsonInputFile);
            // Create JsonReader from Json.
            JsonReader reader = Json.createReader(is);
            // Get the JsonObject structure from JsonReader.
            JsonObject jo = reader.readObject();
            reader.close();
            // read string data
            JsonObject scenarios = (jo.getJsonObject(("scenarios")));
            JsonObject scenario = scenarios.getJsonObject(ScenarioName);
            scenario.keySet().forEach(keyStr -> {

                Object keyvalue = scenario.getString(keyStr);

                scenariodataMap.put(keyStr, keyvalue.toString());

            });

            return scenariodataMap;
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        return null;

    }

    public String PassEncodeDecode(String password, String toggle) {

        String convert = null;
        byte[] message = null;

        if (toggle.equalsIgnoreCase("e")) {
            message = password.getBytes(StandardCharsets.UTF_8);
            convert = Base64.getEncoder().encodeToString(message);
            System.out.println(convert);

        } else if (toggle.equalsIgnoreCase("d")) {

            message = Base64.getDecoder().decode(password);
            convert = new String(message, StandardCharsets.UTF_8);
        }

        return convert;

    }


    public String getNodes(String jsonString, String nodeName) throws ParseException {

        String nodeString = null;
        try {

            return nodeString;

        } catch (Exception pe) {
            pe.printStackTrace();

        }
        return nodeString;
    }


    public ArrayList<String> getFilenames(String[] NameAndCols) {
        String fileListStr = null;

        String[] temStr;

        for (int i = 0; i < NameAndCols.length; i++) {
            temStr = NameAndCols[i].split(",");
            fileListStr = fileListStr + " " + temStr[0].trim();
            mystr.add(temStr[0].trim());

        }
        return mystr;
    }

    public String getFilenameStr(String[] NameAndCols) {
        String fileListStr = null;

        String[] temStr;

        for (int i = 0; i < NameAndCols.length; i++) {
            temStr = NameAndCols[i].split(",");
            fileListStr = fileListStr + " " + temStr[0].trim();
        }
        return fileListStr.trim();
    }

    public String[] updateCurrentFilenames(String[] filesAtStaging, String mystr) {

        String fnameArray = null;
        String[] fArray = null;
        String[] FNArray = mystr.trim().split(",");

        for (int i = 0; i < FNArray.length; i++) {
            for (int j = 0; j < filesAtStaging.length; j++) {
                fnameArray = filesAtStaging[j];
                fArray = fnameArray.split(",");

                if (FNArray[i].contains(fArray[0])) {

                    fArray[0] = FNArray[i];
                    filesAtStaging[i] = String.join(",", fArray);

                }

            }

        }

        return filesAtStaging;
    }


    public void DeleteFromLocal(File Directory, String pattern) {

        File[] files = Directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.getName().contains(pattern)) {
                    file.delete();

                }
            }
        }

    }


}
