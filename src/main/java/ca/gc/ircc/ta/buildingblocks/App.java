package ca.gc.ircc.ta.buildingblocks;


import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.Arrays;
import java.util.Objects;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        MongoClient client = mongoClient();

      //  MongoClient client = mongoClient();

        MongoCursor<String> dbsCursor = client.listDatabaseNames().iterator();
        while(dbsCursor.hasNext()) {
            System.out.println(dbsCursor.next());
        }

        MongoDatabase database = client.getDatabase("demodb");

        System.out.println(database.listCollectionNames());
        MongoCollection<Document> collection = database.getCollection("test1");

        Document document = collection.find().first();
        System.out.println(Objects.requireNonNull(document).toJson());
    }


    public static MongoClient mongoClient() {
        MongoClientOptions options = MongoClientOptions.builder().sslEnabled(false).build();
       return new MongoClient(new MongoClientURI("mongodb://admin:password@mongo-db.ocpintglab.net:27017/"));

        //return new MongoClient(new MongoClient("mongo-db-mvp-sandbox.apps.sbx-aws.ocpintglab.net", options));
}

    }

