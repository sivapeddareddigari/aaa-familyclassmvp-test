package ca.gc.ircc.ta.buildingblocks;


import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.Arrays;


/**
 * Hello world!
 *
 */
public class App1
{
    public static void main( String[] args )
    {

        MongoClient mongoClient = new MongoClient( "mongodb-mvp-sandbox.apps.sbx-aws.ocpintglab.net",27017);
        MongoDatabase database = mongoClient.getDatabase("demodb");
        MongoCollection<Document> collection = database.getCollection("test");
        Document doc = new Document("name", "MongoDB")
                .append("type", "database")
                .append("count", 1)
                .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
                .append("info", new Document("x", 203).append("y", 102));
        collection.insertOne(doc);


    }


    public static MongoClient mongoClient() {
        return new MongoClient(new MongoClientURI("mongodb://admin:password@mongo-db.ocpintglab.net:27017/"));
}

    }

