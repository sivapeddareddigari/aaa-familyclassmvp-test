package ca.gc.ircc.ta.buildingblocks;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteTestData {


    private static final String FILE_NAME = "TestData/Output.xlsx";

	public void addData(String transId, String IdocId, String environment, String poID) {
		Object[][] lodDetails = null;
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Details");
		if (null != transId) {
			lodDetails = new String[][] { { "Element Name", "Value", "Environment" } };
		}
	
		int rowNum = 0;
		System.out.println("Writing/updating excel file...");

		for (Object[] logDetail : lodDetails) {
			Row row = sheet.createRow(rowNum++);
			int colNum = 0;
			for (Object field : logDetail) {
				Cell cell = row.createCell(colNum++);
				if (field instanceof String) {
					cell.setCellValue((String) field);
				} else if (field instanceof Integer) {
					cell.setCellValue((Integer) field);
				}
			}
		}

		try {
			FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
			workbook.write(outputStream);
			workbook.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("...Written/updated excel file");
	}

	
}
