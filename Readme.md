# Test Automation Framework for Testing HIP 

This repository contains a framework designed to automate the functional validation of microservices developed for HIP.
The test automation framework leverages the opensource JAVA frameworks, tools and libraries to validate the functionality at various integration points.

## Implementation of  data driven framework for automating API / UI Testing – using Open Source / Custom framework
![img.png](img.png)

## Folder Structure:
### --> src/main/java 

This folder contains packages with reusable libraries and support functions used across all test scripts

### -->src/main/resources

This folder contains the configuration information for Reeportportal.io 

### -->Src/main/test

This folder contains the packages with wrapper classes ( Test scripts) for a particular functionality or pattern.
The class file will be executed recursively for each data scenario ( data in Testdata file)

### -->TestData

This folder contains the test data files  ( excel Or JSON). 
Note: The test data file can reside in any sub folder but need to be referenced correctly in the test suite xml file.

### -->TestNG-TestSuites

This folder contains the XML files which contains configuration information about the test scripts to be executed, test data file to be used etc.,.
Note: Though it is not mandatory for XML files to be in this folder but it is a best practice to organize the test Suites in this folder.

![img_1.png](img_1.png)

### -->POM.xml
![img_6.png](img_6.png)


This file is the entry point and is parametarized. The test suite name need to be passed as a parameter to POM.XML to avoid hard coding the test suite information in POM.XML file
Ex: <suiteXmlFile>${basedir}/${testNGXMLPath}</suiteXmlFile>
    The full path should be included in the maven build goals as “clean test “-DtestNGXMLPath” since the POM file is parameterized for test data file.
    Ex: clean test "-DtestNGXMLPath=TestNG-TestSuites/familyclassMVP.xml"




### TestData Organization

Testdata is organized in two formats - Excel spreadsheet or JSON. 
### Excel Format:
- The mandatory first Column contains the configuration items that the script is going to use. The worksheet should consist at least one data column, which contains the test data to test a data scenario.
- The rows can be in any order but all cells should contain a value. 
- A test script can be tested for any number of data scenarios and each data scenario should have an additional column. 
- Also, the Data scenarios can either be Positive or Negative based on the input. 


![img_2.png](img_2.png)

### Testsuite file 





![img_3.png](img_3.png)


### Test Results Dashboard - Reportportal.io

The automation framework leverages TestNG framework and ReportPortal.io to publish and visualize the test execution results.
Reportportal.io also provides ML / AI capabilities for further analysis and defect prediction (to be explored)

![img_7.png](img_7.png)
